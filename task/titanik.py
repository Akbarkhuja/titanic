import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def find_title(s):
    flag = False
    res = ""
    for ch in s:
        if ch == ".":
            break
        if ch == ',':
            flag = True
        if flag:
            res += ch
    return res[1::]

def get_filled():
    df = get_titatic_dataframe()
    m_ages = df.groupby(df['Name'].apply(find_title))['Age'].median()

    res = []

    titles = ["Mr.", "Mrs.", "Miss."]
    for t in titles:
        missing_values = df[(df['Name'].str.contains(t)) & (df['Age'].isna())].shape[0]
        age = round(m_ages.get(t, 0))
        result.append((title, missing_values, age))
    return res
